## Rodando o projeto

Para rodar localmente é bem simples basta seguir os passos listados abaixo: 

1-Instalando virtualenv e o git:

Ubuntu:
```
sudo apt-get update
sudo apt-get install python-virtualenv git-core
```

CentOs:
```
yum update
yum install python-virtualenv git
```

2- rodando o projeto:
Acesse um diretório da sua preferência e aplique os seguintes comandos.

```
git clone https://gitlab.com/jotagesales/finances.git
cd finances/web_client
python -m SimpleHTTPServer 5000
```

Pronto, nesse momento a plataforma já está acessível pelo endereço http://127.0.0.1:5000.
