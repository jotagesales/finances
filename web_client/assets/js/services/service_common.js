app.service('CommonService', ['$http', function ($http) {

	this.select_all = function(objects){
		var state_checkbox = $('#ckb_select_all').prop("checked");
		for (var i = 0; i < objects.length; i++) {
			objects[i].selected = state_checkbox;
		}
		return objects;
	}

	this.filter_selecteds = function(objects){
		return objects.filter(function(obj){
			if(obj.selected){
				return obj
			}
		})
	}
	
}]);