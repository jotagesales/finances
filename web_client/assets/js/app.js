var app = angular.module("finance", ["ngRoute"])

app.config(['$routeProvider', '$httpProvider',function($routeProvider, $httpProvider) {


	$routeProvider.otherwise({ redirectTo: '/dashboard' })

}]);