app.config(['$routeProvider', function ($routeProvider) {
	
	$routeProvider
	.when('/dashboard', {
		templateUrl: 'templates/dashboard.html',
		controller: 'dashboardCtrl'
	})
	.when('/counts', {
		templateUrl: 'templates/counts.html',
		controller: 'CountsCtrl'
	})
	.when('/transactions', {
		templateUrl: 'templates/transactions.html',
		controller: 'TransactionsCtrl'
	})
	.when('/favored', {
		templateUrl: 'templates/favored.html',
		controller: 'FavoredCtrl'
	});
}])
