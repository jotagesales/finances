app.controller('FavoredCtrl', ['$scope', 'CommonService', function ($scope, CommonService) {

	$scope.favoreds = [
		{name:'Midia works', document:'3242342432', address:'Rua aliança liberal, 165, Bela Aliança, São Paulo'},
		{name:'Midia works', document:'3242342432', address:'Rua aliança liberal, 165, Bela Aliança, São Paulo'},
		{name:'Midia works', document:'3242342432', address:'Rua aliança liberal, 165, Bela Aliança, São Paulo'},
		{name:'Midia works', document:'3242342432', address:'Rua aliança liberal, 165, Bela Aliança, São Paulo'},
		{name:'Midia works', document:'3242342432', address:'Rua aliança liberal, 165, Bela Aliança, São Paulo'},
		{name:'Projeto Maior', document:'3242342432', address:'Rua aliança liberal, 165, Bela Aliança, São Paulo'},
		{name:'Eduardo Coelho', document:'3242342432', address:'Rua aliança liberal, 165, Bela Aliança, São Paulo'},
		{name:'Midia works', document:'3242342432', address:'Rua aliança liberal, 165, Bela Aliança, São Paulo'},
	]

	$scope.select_all = function(objects){
		return CommonService.select_all(objects)
	}

	$scope.filter_selecteds = function(objects){
		return CommonService.filter_selecteds(objects);
	}

	$scope.has_selected_items = function(objects){
		var selected_items = CommonService.filter_selecteds(objects);
		if(selected_items.length > 0){
			return true;
		}
		return false;
	}
	
}])