app.controller('CountsCtrl', ['$scope', 'CommonService', function ($scope, CommonService) {

	$scope.counts = [
		{name:'Bradesco', description:'Conta do brandesco', initial_value:100, initial_date:"01/01/2016"},
		{name:'Itau', description:'Conta do itau', initial_value:0, initial_date:"01/01/2016"},
		{name:'Santander', description:'Conta do Santander', initial_value:0, initial_date:"01/01/2016"},
	];

	$scope.select_all = function(objects){
		return CommonService.select_all(objects)
	}

	$scope.filter_selecteds = function(objects){
		return CommonService.filter_selecteds(objects);
	}

	$scope.has_selected_items = function(objects){
		var selected_items = CommonService.filter_selecteds(objects);
		if(selected_items.length > 0){
			return true;
		}
		return false;
	}
	
}]);