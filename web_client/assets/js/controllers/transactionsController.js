app.controller('TransactionsCtrl', ['$scope', 'CommonService', function ($scope, CommonService) {

	$scope.transactions = [
		{type:'Débito', description:'Pagamento fogão', value:'R$ 407,00', date:'01/04/2016'},
		{type:'Crédito', description:'Salário', value:'R$ 1000,00', date:'01/04/2016'},
		{type:'Crédito', description:'Pagamento fogão', value:'R$ 407,00', date:'01/04/2016'},
		{type:'Crédito', description:'Pagamento fogão', value:'R$ 407,00', date:'01/04/2016'},
	]

	$scope.select_all = function(objects){
		return CommonService.select_all(objects)
	}

	$scope.filter_selecteds = function(objects){
		return CommonService.filter_selecteds(objects);
	}

	$scope.has_selected_items = function(objects){
		var selected_items = CommonService.filter_selecteds(objects);
		if(selected_items.length > 0){
			return true;
		}
		return false;
	}
}])