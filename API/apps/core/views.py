from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import ContaSerializer,  TransacaoSerializer
from .models import Conta, Transacao


# Create your views here.


class ContaView(APIView):

    serializer_class = ContaSerializer

    def get(self, request, id=None, format=None):
        if id is not None:
            contas = get_object_or_404(Conta, pk=id)
            many = False
        else:
            contas = Conta.objects.all()
            many = True

        serialize = self.serializer_class(contas, many=many)

        return Response(serialize.data)


class TransacaoView(APIView):

    serializer_class = TransacaoSerializer

    def get(self, request, id=None, format=None):
        if id is not None:
            transacoes = get_object_or_404(Transacao, pk=id)
            many = False
        else:
            transacoes = Transacao.objects.all()
            many = True

        serialize = self.serializer_class(transacoes, many=many)

        return Response(serialize.data)
