
from rest_framework import serializers
from .models import Conta, Transacao


class ContaSerializer(serializers.ModelSerializer):

	class Meta:
		model = Conta
		fields = ('id', 'name', 'description', 'initial_value')


class TransacaoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Transacao
        fields = ('id', 'description', 'type_transaction', 'value', 'date')
