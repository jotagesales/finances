# -*- coding:UTF-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Conta(models.Model):

	name = models.CharField('Nome', max_length=50)
	description = models.TextField()
	initial_value = models.FloatField()

	class Meta:
		db_table = 'Contas'
		verbose_name = "Conta"
		verbose_name_plural = "Contas"


	def __str__(self):
		return '{0} - {1}'.format(self.id, self.name)


class Transacao(models.Model):

	CREDITO = 'C'
	DEBITO = 'D'

	TYPE_TRANSACTION_CHOICES = (
		(CREDITO, 'Débito'),
		(DEBITO, 'Crédito'),

		)

	description = models.CharField(max_length=150)
	type_transaction = models.CharField(max_length=1, choices=TYPE_TRANSACTION_CHOICES)
	value = models.FloatField()
	date = models.DateField()

	class Meta:
		db_table = 'Transacoes'
		verbose_name = "Transação"
		verbose_name_plural = "Transações"

	def __str__(self):
		return self.id
